<?php

namespace App\Http\Controllers;

use Auth;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function store(Request $request) {

        Auth::user()->posts()->save(new Post(['content' => $request->input('content')]));

        /*return Post::create([
            'user_id' => Auth::id(),
            'content' => $request->input('content')
        ]);*/
    }
}
