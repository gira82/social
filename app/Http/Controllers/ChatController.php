<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ChatMessage;
use Auth;
use App\Events\ChatMessageWasReceived;

class ChatController extends Controller
{
    public function message(Request $request) {
        $user = Auth::user();

        $message = ChatMessage::create([
            'user_id' => $user->id,
            'message' => $request->input('message')
        ]);


        $users = $user->friends();

        foreach ($users as $u) {
            // if ($u->id <> $user->id) {
            User::find($u->id)->notify(new \App\Notifications\NewChatMessage($user, $message));
            //}

        }

        event(new ChatMessageWasReceived($message, $user));

        return 1;
    }
}
