<?php

namespace App\Http\Controllers;

//use Illuminate\Contracts\Session\Session;
use App\User;
use Illuminate\Http\Request;
use Auth;

class ProfileController extends Controller
{
    public function index($slug)
    {
        $user = User::where('slug', $slug)->firstOrFail();
        return view('profiles.profile')->with(compact('user'));
    }

    /*public function edit()
    {
        return view('admin.profiles.edit')->with('info', Auth::user()->profile);
    }*/

    public function edit()
    {
        return view('profiles.edit')->with('info', Auth::user()->profile);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'location' => 'required',
            'about' => 'required|max:255'
        ]);

        Auth::user()->profile()->update([
            'location' => $request->location,
            'about' => trim($request->about)
        ]);


        if ($request->hasFile('avatar')) {
            Auth::user()->update([
                'avatar' => $request->avatar->store('public/avatars')
            ]);
        }

        //Session::flash('success', 'Profile updated');
        $request->session()->flash('success', 'Profile Updated');
        //flash()->success('Profile Updated');
        return redirect()->back();
    }
}
