<?php
/**
 *  VueTables server-side component interface
 */

namespace App\Services\VueTables;

use Illuminate\Http\Request;

Interface VueTablesInterface {

  public function get($table, Array $fields, Request $request);

}
