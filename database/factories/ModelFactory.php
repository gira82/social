<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'gender' => 0,
        'avatar' => 'public/defaults/avatars/female.png',
        'remember_token' => str_random(10),
        'api_token' => str_random(60),
    ];
});

$factory->define(\App\Profile::class, function (Faker\Generator $faker) {
    return [
        'location' => $faker->city,
        'about' =>$faker->paragraphs(4, true)
    ];
});
