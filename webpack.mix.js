const { mix } = require('laravel-mix');
var webpack = require('webpack');
//var path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
    plugins: [
        new webpack.ProvidePlugin({
            "window.Tether": 'tether',
            $: "jquery",
            jQuery: "jquery"
        })
    ],
    module: {
        loaders: [
            {
                test: /datatables\.net.*/,
                loader: 'imports?define=>false'
            },

        ],
        rules: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules(?!\/(vue-tables-2|vue-pagination-2))/,
                query: {
                    presets: ['es2015'],
                    plugins: ["transform-vue-jsx"]
                }
            }
        ]
    }
});



mix.js([
    'resources/assets/admin/js/app.js',
    //'resources/assets/plugins/jquery/jquery.js',
    'resources/assets/plugins/bootstrap/tether.js',
    //'resources/assets/plugins/bootstrap/bootstrap.js',
    'resources/assets/plugins/customScrollBar/customScrollBar.js',

    // ** Additional Plugins **
    'resources/assets/plugins/ladda/spin.js',
    'resources/assets/plugins/ladda/ladda.js',
    // 'resources/assets/plugins/toastr/toastr.js',
    // 'resources/assets/plugins/notie/notie.js',
    'resources/assets/plugins/jquery-validate/jquery.validate.js',
    'resources/assets/plugins/jquery-validate/additional-methods.js',
    'resources/assets/plugins/clockpicker/bootstrap-clockpicker.js',
    'resources/assets/plugins/switchery/switchery.js',
    'resources/assets/plugins/select2/select2.js',
    //'resources/assets/plugins/datatables/dataTables.min.js',
    //'resources/assets/plugins/datatables/dataTables.bootstrap.js',
    'resources/assets/plugins/multiselect/jquery.multi-select.js',
    'resources/assets/plugins/bootstrapSelect/bootstrap-select.js',
    'resources/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js',
    'resources/assets/plugins/timepicker/jquery.timepicker.js',
    //'resources/assets/plugins/summernote/summernote.js',
    'resources/assets/plugins/simplemde/simplemde.min.js',
    'resources/assets/plugins/Chartjs/Chart.js',
    'resources/assets/plugins/alertify/alertify.js',
    'resources/assets/plugins/easypiecharts/jquery.easypiechart.js',

    // ** Laraspace Inits **
    // 'resources/assets/plugins/laraspace/laraspace-layout.js',
    // 'resources/assets/plugins/laraspace/laraspace-notifs.js',
    // 'resources/assets/plugins/laraspace/laraspace-forms.js'


    ], 'public/assets/admin/js/core/main.js').sourceMaps()
    .sass('resources/assets/admin/sass/laraspace.scss', 'public/assets/admin/css/laraspace.css').sourceMaps();
    //.version();


/* frontend */
mix.js('resources/assets/js/app.js', 'public/js').sourceMaps()
  .sass('resources/assets/sass/app.scss', 'public/css').sourceMaps();
   //.version();





/*

mix.webpackConfig({
    //context: __dirname + '/resources/assets/admin',
    entry: "resources/assets/admin/",
});


mix.sass('laraspace.scss', 'public/assets/admin/css/laraspace.css');


mix.js([
    // ** Required Plugins **
    'jquery/jquery.js',
    'bootstrap/tether.js',
    'bootstrap/bootstrap.js',
    'customScrollBar/customScrollBar.js',

    // ** Additional Plugins **
    'ladda/spin.js',
    'ladda/ladda.js',
    'toastr/toastr.js',
    'notie/notie.js',
    'jquery-validate/jquery.validate.js',
    'jquery-validate/additional-methods.js',
    'clockpicker/bootstrap-clockpicker.js',
    'switchery/switchery.js',
    'select2/select2.js',
    'datatables/dataTables.min.js',
    'datatables/dataTables.bootstrap.js',
    'multiselect/jquery.multi-select.js',
    'bootstrapSelect/bootstrap-select.js',
    'bootstrap-datepicker/bootstrap-datepicker.js',
    'timepicker/jquery.timepicker.js',
    'summernote/summernote.js',
    'simplemde/simplemde.min.js',
    'Chartjs/Chart.js',
    'alertify/alertify.js',
    'easypiecharts/jquery.easypiechart.js',

    // ** Laraspace Inits **
    'laraspace/laraspace-layout.js',
    'laraspace/laraspace-notifs.js',
    'laraspace/laraspace-forms.js'

],'public/assets/admin/js/core/plugins.js','./resources/assets/plugins');



*/


// mix.js('pages/dashboard.js', 'public/assets/admin/js/pages/dashboard.js');
// mix.js('pages/todos.js', 'public/assets/admin/js/pages/todos.js');

/*
mix.webpackConfig({
    resolve: {
        modules: [
            path.resolve(__dirname, 'resources/assets/front/')
        ]
    }
});

//FRONT
mix.sass('front.scss', 'public/assets/front/css/front.css');
*/





