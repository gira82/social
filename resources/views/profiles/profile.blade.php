@extends(('layouts.app'))

@section('content')
    <div class="container">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="row">
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <p class="text-center">
                                {{ $user->full_name  }}'s Profile.
                            </p>
                        </div>
                        <div class="panel-body text-center">
                            <img src="{{ $user->avatar }}" width="140px" style="border-radius: 50%; margin: 10px;" alt="avatar">
                            <p class="text-center">{{ $user->profile->location }}</p>
                            <p class="text-center">
                                @if(Auth::id() == $user->id)
                                    <a href="{{ route('profile.edit') }}" class="btn btn-info">Edit your Profile</a>
                                @endif
                            </p>
                        </div>
                    </div>
                    @if(Auth::id() !== $user->id)
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <friend :profile_user_id="{{$user->id}}"></friend>
                            </div>
                        </div>
                    @endif
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <p class="text-center">
                                About me
                            </p>
                        </div>
                        <div class="panel-body text-center">
                            <p class="text-center">
                                {{ $user->profile->about  }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
