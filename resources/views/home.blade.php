@extends('layouts.app')

@section('content')
    @if(Auth::check())
        <post txt="{{ __('messages.postbutton') }}"></post>
        <feed></feed>
    @else
        <div class="container ">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            {{ __('messages.hometxt') }}.
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endif
@endsection
