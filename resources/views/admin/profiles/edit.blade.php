@extends('admin.layouts.layout-basic')


@section('content')
    <div class="main-content">

        <div class="page-header">
            <h3 class="page-title">Profile</h3>
            <ol class="breadcrumb">
                <!-- <li><a href="{ { ro ute('admin.dashboard') } }">Home</a></li>-->
                <li><a href="{{route('admin.home')}}">Home</a></li>
                <li class="active">Profile</li>
            </ol>
            <div class="page-actions">
                <a href="#" class="btn btn-primary"><i class="fa fa-plus"></i> New User</a>
                <button class="btn btn-danger"><i class="fa fa-trash"></i> Delete </button>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h6>Edit your Profile</h6>
                        <div class="card-actions">
                        </div>
                    </div>
                    <div class="card-block">
                        <form action="{{ route('admin.profile.update') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="location">Location</label>
                                <input type="text" name="location"value="{{ $info->location }}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="about">About me</label>
                                <textarea name="about" class="form-control" cols="30" rows="10" required>{{ $info->about  }}</textarea>
                            </div>
                            <div class="form-group">
                                <p class="text-center">
                                    <button class="btn btn-primary btn-lg" type="submit" >
                                        Save your informations
                                    </button>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
