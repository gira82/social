@extends('admin.layouts.layout-basic')

@section('scripts')
    <!-- <scr ipt src="/assets/admin/js/users/users.js"></scr ipt> -->
@stop

@section('content')
    <div class="main-content">

        <div class="page-header">
            <h3 class="page-title">{{__('menu.users')}}</h3>
            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}">{{__('menu.home')}}</a></li>
                <li class="active">{{__('menu.users')}}</li>
            </ol>
            <!-- <div class="page-actions">
                <a href="#" class="btn btn-primary"><i class="fa fa-plus"></i> New User</a>
                <button class="btn btn-danger"><i class="fa fa-trash"></i> Delete </button>
            </div> -->
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h6>{{__('menu.users')}}</h6>

                        <div class="card-actions">

                        </div>
                    </div>
                    <div class="card-block">
                        <user-table
                                txtfromto="@lang('vue-table.fromTo')"
                                txtfilter="@lang('vue-table.filter')"
                                txtquery="@lang('vue-table.query')"
                                txtelpage="@lang('vue-table.elpage')"
                        ></user-table>
                        <!-- <div>
                            <my-vuetable></my-vuetable>
                        </div> -->
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop
