@extends('admin.layouts.layout-basic')

@section('scripts')
    <script src="/assets/admin/js/users/users.js"></script>
@stop

@section('content')
    <div class="main-content">

        <div class="page-header">
            <h3 class="page-title">Dashboard</h3>
            <ol class="breadcrumb">
                <li class="active"><a href="{{ route('admin.home') }}">Home</a></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h6>Dashboard</h6>

                        <div class="card-actions">

                        </div>
                    </div>
                    <div class="card-block">

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
