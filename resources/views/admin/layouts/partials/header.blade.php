<header class="site-header">
  <a href="{{ url('/') }}" class="brand-main">
    <img src="/assets/admin/img/logo-desk.png" id="logo-desk" alt="Laraspace Logo" class="hidden-sm-down">
    <img src="/assets/admin/img/logo-mobile.png" id="logo-mobile" alt="Laraspace Logo" class="hidden-md-up">
  </a>
  <a href="#" class="nav-toggle">
    <div class="hamburger hamburger--htla">
      <span>toggle menu</span>
    </div>
  </a>

    <ul class="action-list">
      @include('admin.layouts.partials.language')
      <!-- <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-plus"></i></a>
        <div class="dropdown-menu dropdown-menu-right">
          <a class="dropdown-item" href="#"><i class="fa fa-edit"></i> New Post</a>
          <a class="dropdown-item" href="#"><i class="fa fa-tag"></i> New Category</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#"><i class="fa fa-star"></i> Separated link</a>
        </div>
      </li>
      <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell"></i></a>
        <div class="dropdown-menu dropdown-menu-right notification-dropdown">
          <h6 class="dropdown-header">Notifications</h6>
          <a class="dropdown-item" href="#"><i class="fa fa-user"></i> New User was Registered</a>
          <a class="dropdown-item" href="#"><i class="fa fa-comment"></i> A Comment has been posted.</a>
        </div>
      </li> -->
      <li>
        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="avatar">
            <img src="{{ Auth::user()->avatar }}" alt="Avatar"></a>
            <div class="dropdown-menu dropdown-menu-right notification-dropdown">
                <a class="dropdown-item" href="{{ route('admin.profile.edit')}}"><i class="fa fa-cogs"></i> Settings</a>
                <!-- <a class="dropdown-item" href="#"><i class="fa fa-sign-out"></i> Logout</a> -->
                <a href="{{ url('/logout') }}"
                   onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out"></i> Logout
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
      </li>
    </ul>
</header>
