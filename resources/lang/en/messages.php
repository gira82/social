<?php

return [

    'password-reset-button' => 'Send Password Reset Link',
    'language' => 'Lingua',
    'loggedd-in' => 'You are logged in!',
    'register' => 'Register',
    'hometxt' => 'Login to see wonderful contents!',
    'myprofile' => 'My Profile',
    'unreadnots' => 'Unread Notifications',
    'searchusers' => 'Search for other users',
    'chat' => 'Chat something here...',
    'postbutton' => 'Create a post',
];
