<?php

return [
    'fromTo' => 'Showing {from} to {to} of {count} records|{count} records',
    'filter' => 'Filter Results:',
    'query' => 'Query search',
    'elpage' => 'Records:'
];
