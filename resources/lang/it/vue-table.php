<?php

return [
    'fromTo' => 'da {from} a {to} di {count} elementi|{count} Elementi|Un elemento',
    'filter' => 'Filtra i risultati:',
    'query' => 'Cerca',
    'elpage' => 'Elementi per pagina:'
];
