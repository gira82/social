<?php

return [

    'password-reset-button' => 'Invia il link per il reset della password',
    'language' => 'Lingua',
    'loggedd-in' => 'Ti sei loggato!',
    'register' => 'Registrati',
    'hometxt' => 'Fai il login per vedere contenuti fantastici!',
    'myprofile' => 'Profilo',
    'unreadnots' => 'Notifiche non lette',
    'searchusers' => 'Cerca altri utenti',
    'chat' => 'Chatta qui...',
    'postbutton' => 'Crea un post',
];
