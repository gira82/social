
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */




// Vue.component('example', require('./components/Example.vue'));



Vue.component('init', require('./components/Init.vue'));
Vue.component('userTable', require('./components/UserTable.vue'));
Vue.component('my-vuetable', require('./components/MyVuetable.vue'));

// autenticazione?
// Vue.http.headers.common['Authorization'] = auth.getAuthHeader();
// Vue.http.headers.common['Authorization'] = 'Bearer 1kvwY14EV4Iy3R3PPr7VEchBlJSgQu0nH0meaqBmmdbgYbwLCU1XmkwnwwHh';


import { store } from './store/store';

const app = new Vue({
    el: '#app',
    data: {
    },
    store
});

