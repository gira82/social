/**
 * Created by stefanogiraldo on 01/02/17.
 */
export default {
    SETAPITOKEN: (state, payload) => {
        state.user.api_token = payload;
    },
    SETUSER: (state, payload) => {
        state.user = payload;
    }
};