/**
 * Created by stefanogiraldo on 01/02/17.
 */
// vuex

import Vue from 'vue'
import Vuex from 'vuex';
import getters from './getters';
import mutations from './mutations';
import actions from './actions';

Vue.use(Vuex);



export const store = new Vuex.Store({
   state: {
       user: {},
   },
    getters,
    mutations,
    actions
});

