/**
 * Created by stefanogiraldo on 01/02/17.
 */
export default {
    setApiToken: ({commit}, payload) => {
        commit('SETAPITOKEN', payload)
    },
    setUser: ({commit}, payload) => {
        commit('SETUSER', payload)
    }
};