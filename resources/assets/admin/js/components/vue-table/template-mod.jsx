module.exports = function(source) {
  return function(h) {

    var rows = require('vue-tables-2/lib/template/rows.jsx')(h, this)
    var normalFilter = require('vue-tables-2/lib/template/normal-filter.jsx')(h, this)
    var dropdownPagination = require('vue-tables-2/lib/template/dropdown-pagination.jsx')(h, this)
    var columnFilters = require('vue-tables-2/lib/template/column-filters.jsx')(h, this);
    var footerHeadings = require('vue-tables-2/lib/template/footer-headings.jsx')(h, this);
    var noResults = require('vue-tables-2/lib/template/no-results.jsx')(h, this);
    var pagination = require('vue-tables-2/lib/template/pagination.jsx')(h, this);
    var dropdownPaginationCount = require('vue-tables-2/lib/template/dropdown-pagination-count.jsx')(h, this);
    var headings = require('vue-tables-2/lib/template/headings.jsx')(h, this);
    var perPage = require('vue-tables-2/lib/template/per-page.jsx')(h, this);

    return <div class={"VueTables VueTables--" + this.source}>
      <div class="row">
        <div class="col-md-6">
          {normalFilter}
        </div>
        <div class="col-md-6">
          {dropdownPagination}
          {perPage}
        </div>
      </div>
      <table class={'VueTables__table table ' + this.opts.skin}>
        <thead>
          <tr>
            {headings}
          </tr>
          {columnFilters}
        </thead>
        {footerHeadings}
        <tbody>
          {noResults}
          {rows}
        </tbody>
      </table>
      {pagination}
      {dropdownPaginationCount}
    </div>
  }
}
