/**
 * Created by stefanogiraldo on 01/02/17.
 */
export default {
    getApiToken: state => {
        return state.user.api_token;
    },
    getUser: state => {
        return state.user;
    },
    all_nots(state) {
        return state.nots;
    },
    all_nots_count(state) {
        return state.nots.length;
    },
    all_post(state) {
        return state.posts;
    },
    user_id(state) {
        return state.auth_user.id
    }
};