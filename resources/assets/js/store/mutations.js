/**
 * Created by stefanogiraldo on 01/02/17.
 */
export default {
    SETAPITOKEN: (state, payload) => {
        state.user.api_token = payload;
    },
    SETUSER: (state, payload) => {
        state.user = payload;
    },
    add_not(state, not) {
        state.nots.push(not);
    },
    add_post(state, post) {
        state.posts.push(post);
    },
    auth_user_data(state, user) {
        state.auth_user = user;
    },
    update_post_likes(state, payload) {
        let post = state.posts.find( (p) => {
            return p.id === payload.id;
        });
        post.likes.push(payload.like);
    },
    unlike_post(state, payload) {
        let post = state.posts.find( (p) => {
            return p.id === payload.post_id;
        });
        let like = post.likes.find( (l) => {
            return l.id === payload.like_id;
        });
        let index = post.likes.indexOf(like);
        post.likes.splice(index, 1);

    }
};