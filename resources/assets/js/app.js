
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */




// Vue.component('example', require('./components/Example.vue'));



/*Vue.component(
    'userTable',
    require('./components/UserTable.vue')
);

Vue.component(
    'init',
    require('./components/Init.vue')
)*/

// autenticazione?
// Vue.http.headers.common['Authorization'] = auth.getAuthHeader();
// Vue.http.headers.common['Authorization'] = 'Bearer 1kvwY14EV4Iy3R3PPr7VEchBlJSgQu0nH0meaqBmmdbgYbwLCU1XmkwnwwHh';

import moment from 'moment';
import { store } from './store/store';


Vue.component('friend', require('./components/Friend.vue'));
Vue.component('notification', require('./components/Notification.vue'));
Vue.component('unread', require('./components/UnreadNots.vue'));
Vue.component('post', require('./components/Post.vue'));
Vue.component('feed', require('./components/Feed.vue'));
Vue.component('init', require('./components/Init.vue'));
Vue.component('search', require('./components/Search.vue'));
Vue.component('chat', require('./components/Chat.vue'));

Vue.filter('timeago', function(value) {
    return moment(String(value)).format('hh:mm');
});

const app = new Vue({
    el: '#app',
    data: {
    },
    store
});

