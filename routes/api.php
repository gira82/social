<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/*Route::middleware('auth')->get('users', [
    'uses' => 'UserController@tableIndex',
    'as' => 'users.tableIndex'
]);*/

Route::middleware('auth:api')->get('users', [
    'uses' => 'UserController@tableIndex',
    'as' => 'users.tableIndex'
]);

Route::middleware('auth:api')->get('users2', [
    'uses' => 'UserController@table2Index',
    'as' => 'users.table2Index'
]);