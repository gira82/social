 <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', [
    'uses' => 'HomeController@index',
    'as' => 'home'
]);


Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('lang/{lang}', [
    'uses'=>'LanguageController@switchLang',
    'as'=>'lang.switch'
]);

Route::group([
    'prefix' => 'admin',
    'middleware' => ['role:superadministrator']
], function() {

    Route::get('/', [
        'uses' => 'AdminController@index',
        'as' => 'admin.home'
    ]);
    Route::resource('users', 'UserController');
    Route::get('profile/edit', [
        'uses' => 'ProfileController@edit',
        'as' => 'admin.profile.edit'
    ]);
    Route::post('profile/edit', [
        'uses' => 'ProfileController@update',
        'as' => 'admin.profile.update'
    ]);

});

Route::group([
    'middleware' => 'auth'
], function () {
    Route::get('/profile/{slug}', [
        'uses' => 'ProfileController@index',
        'as' => 'profile'
    ]);

    Route::get('/profile/edit/profile', [
        'uses' => 'ProfileController@edit',
        'as' => 'profile.edit'
    ]);

    Route::post('/profile/update/profile', [
        'uses' => 'ProfileController@update',
        'as' => 'profile.update'
    ]);

    Route::get('/add_friend/{id}', [
        'uses' => 'FriendshipController@add_friend',
        'as' => 'add_friend'
    ]);

    Route::get('/accept_friend/{id}', [
        'uses' => 'FriendshipController@accept_friend',
        'as' => 'accept_friend'
    ]);

    Route::get('check_relationship_status/{id}', [
        'uses' => 'FriendshipController@check',
        'as' => 'check'
    ]);


    Route::get('get_unread', function () {
        return Auth::user()->unreadNotifications;
    });

    Route::get('/notifications/', [
        'uses' => 'HomeController@notifications',
        'as' => 'notifications'
    ]);


    Route::post('/create/post', [
        'uses' => 'PostController@store'
    ]);


    Route::get('/feed/', [
        'uses' => 'FeedController@feed',
        'as' => 'feed'
    ]);


    Route::get('/get_auth_user_data', function() {
        return Auth::user();
    });

    Route::get('/like/{id}', [
        'uses' => 'LikeController@like',
        'as' => 'like'
    ]);

    Route::get('/unlike/{id}', [
        'uses' => 'LikeController@unlike',
        'as' => 'unlike'
    ]);

    // Send a message by Javascript.
    Route::post('/message', [
        'uses' => 'ChatController@message',
        'as' => 'message'
    ]);


});

